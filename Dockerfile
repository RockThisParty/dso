FROM golang:1.16-alpine as build
WORKDIR /app
COPY . .
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o app app.go

# golang image where workspace (GOPATH) configured at /go.
FROM alpine:latest

# Copy the local package files to the container’s workspace.
COPY --from=build /app/app app

# http server listens on port 8080.
EXPOSE 8080

CMD ["/app"]