package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

func helloHandler(res http.ResponseWriter, req *http.Request) {
	fmt.Println("Enter /hello")
	res.Header().Set(
		"Content-Type",
		"text/html",
	)
	io.WriteString(
		res,
		`<doctype html>
<html>
	<head>
		<title>Hello Bro</title>
	</head>
	<body>
		Hello Bro </br>
		This a DSO Demo Project. https://gitlab.com/RockThisParty/dso
	</body>
</html>`,
	)
}
func defaultHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Enter /")
	fmt.Fprintf(w, "Go web app powered by Docker")
}
func main() {
	fmt.Println("Start")
	http.HandleFunc("/", defaultHandler)
	http.HandleFunc("/hello", helloHandler)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
		return
	}
}
